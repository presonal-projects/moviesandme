const API_TOKEN = ' YOUR API KEY FROM https://www.themoviedb.org/ ';

export const getMoviesFromApiWithSearchedText = (text, page) => {
  const url =
    'https://api.themoviedb.org/3/search/movie?api_key=' +
    API_TOKEN +
    '&language=en-US&query=' +
    text +
    '&page=' +
    page;

  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
};

export const getPopularMoviesFromApi = () => {
  const url =
    'https://api.themoviedb.org/3/movie/popular?api_key=' +
    API_TOKEN +
    '&language=en-US';

  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
};

export const getImageFromApi = (name) => {
  return 'https://image.tmdb.org/t/p/w500' + name;
};

export const getMovieDetailFromApi = (id) => {
  const url =
    'https://api.themoviedb.org/3/movie/' +
    id +
    '?api_key=' +
    API_TOKEN +
    '&language=en-US';

  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
};
