import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { AntDesign } from '@expo/vector-icons';

import MovieList from './MovieList';
import colors from '../Constant/color';

class Favorites extends Component {
  constructor(props) {
    super(props);
  }

  _displayDetailForFilm = (id, title) => {
    this.props.navigation.navigate('Detail', {
      movieId: id,
      movieTitle: title,
    });
  };

  _displayNoFavorites = () => {
    if (this.props.favoritesFilm.length === 0) {
      return (
        <View style={styles.noFavorites}>
          <AntDesign name='exclamationcircleo' size={30} color={colors.blue} />
          <Text style={styles.noResultText}>No favorites saved</Text>
        </View>
      );
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <MovieList
          films={this.props.favoritesFilm}
          navigation={this.props.navigation}
          favoriteList={true}
        />
        {this._displayNoFavorites()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
  },
  noFavorites: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noResultText: {
    marginTop: 10,
  },
  title: {
    fontFamily: 'PT',
  },
});

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.favoritesFilm,
  };
};

export default connect(mapStateToProps)(Favorites);
