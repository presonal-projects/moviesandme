import React, { Component } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import MovieItem from './MovieItem';
import { connect } from 'react-redux';

class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      films: [],
    };
  }

  _displayDetailForFilm = (id, title) => {
    this.props.navigation.navigate('Detail', {
      movieId: id,
      movieTitle: title,
    });
  };

  render() {
    return (
      <FlatList
        style={styles.list}
        data={this.props.films}
        extraData={this.props.favoritesFilm}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <MovieItem
            movie={item}
            isFavorite={
              this.props.favoritesFilm.findIndex(
                (film) => film.id === item.id
              ) !== -1
                ? true
                : false
            }
            displayDetailForFilm={this._displayDetailForFilm}
          />
        )}
        onEndReachedThreshold={0.5}
        onEndReached={() => {
          if (
            !this.props.favoriteList &&
            this.props.page < this.props.totalPages
          ) {
            this.props.loadFilms();
          }
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.favoritesFilm,
  };
};

export default connect(mapStateToProps)(MovieList);
