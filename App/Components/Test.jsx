import React, { Component } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import Hello from './Hello';

export default class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topPosition: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.spring(this.state.topPosition, {
      toValue: 100,
      speed: 4,
      bounciness: 30,
    }).start();
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={[styles.subContainer, { top: this.state.topPosition }]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    backgroundColor: 'red',
    width: 100,
    height: 100,
  },
});
