import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  Platform,
  Share,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import numeral from 'numeral';
import moment from 'moment';
import { connect } from 'react-redux';
import { AntDesign, Ionicons } from '@expo/vector-icons';

import { getMovieDetailFromApi, getImageFromApi } from '../API/TMDB';
import colors from '../Constant/color';
import EnlargeShrink from '../Animation/EnlargeShrink';

class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      film: undefined,
    };
  }

  componentDidMount() {
    this.setState({ isReady: false });
    getMovieDetailFromApi(this.props.route.params.movieId).then((data) => {
      this.setState({
        isReady: true,
        film: data,
      });
    });
    if (Platform.OS === 'ios') {
      this.props.navigation.setOptions({
        headerRight: () => (
          <TouchableOpacity
            onPress={() => this._shareFilm()}
            style={styles.shareButtonIos}
          >
            <Ionicons name='ios-share-outline' size={24} color={colors.white} />
          </TouchableOpacity>
        ),
      });
    }
  }

  _toggleFavorite() {
    const action = { type: 'TOGGLE_FAVORITE', value: this.state.film };
    this.props.dispatch(action);
  }

  _displayFavoriteImage() {
    var sourceImage = require('../Assets/Images/ic_favorite_border.png');
    var shouldEnlarge = false;

    if (
      this.props.favoritesFilm.findIndex(
        (item) => item.id === this.state.film.id
      ) !== -1
    ) {
      sourceImage = require('../Assets/Images/ic_favorite.png');
      var shouldEnlarge = true;
    }

    return (
      <EnlargeShrink shouldEnlarge={shouldEnlarge}>
        <Image style={styles.favoriteImage} source={sourceImage} />
      </EnlargeShrink>
    );
  }

  _displayFilm() {
    const film = this.state.film;
    if (film != undefined) {
      return (
        <ScrollView style={styles.scrollviewContainer}>
          <Image
            source={{ uri: getImageFromApi(film.backdrop_path) }}
            style={styles.image}
          />
          <View style={styles.viewContainer}>
            <Text style={styles.title}>{film.title}</Text>
            <TouchableOpacity
              style={styles.favoriteContainer}
              onPress={() => this._toggleFavorite()}
            >
              {this._displayFavoriteImage()}
            </TouchableOpacity>
            <Text style={styles.overview}>{film.overview}</Text>
            <Text style={styles.infos}>
              Release date : {moment(film.release_date).format('LL')}
            </Text>
            <Text style={styles.infos}>Rated : {film.vote_average}/10</Text>
            <Text style={styles.infos}>
              Number of votes : {film.vote_count}
            </Text>
            <Text style={styles.infos}>
              Budget : {numeral(film.budget).format('0,0[.]00 $')}
            </Text>
            <Text style={styles.infos}>
              Genres : {film.genres.map((key) => key.name).join(' / ')}
            </Text>
            <Text style={styles.infos}>
              Company :{' '}
              {film.production_companies.map((key) => key.name).join(' / ')}
            </Text>
          </View>
        </ScrollView>
      );
    }
  }

  _displayLoading() {
    if (!this.state.isReady) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={colors.blue} size='large' />
        </View>
      );
    }
  }

  _shareFilm() {
    const { film } = this.state;
    Share.share({ title: film.title, message: film.overview });
  }

  _displayFloatingActionBottom() {
    const { film } = this.state;
    if (film != undefined && Platform.OS === 'android') {
      return (
        <TouchableOpacity
          style={styles.shareButtonAndroid}
          onPress={() => this._shareFilm()}
        >
          <AntDesign name='sharealt' size={24} color={colors.white} />
        </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this._displayFilm()}
        {this._displayLoading()}
        {this._displayFloatingActionBottom()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollviewContainer: {
    flex: 1,
  },
  viewContainer: {
    marginHorizontal: 10,
  },
  loadingContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: 200,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'PT',
    marginVertical: 15,
  },
  overview: {
    fontFamily: 'PT',
    textAlign: 'justify',
    marginVertical: 15,
  },
  infos: {
    fontFamily: 'PT_Bold',
    marginVertical: 2,
  },
  favoriteContainer: {
    alignItems: 'center',
  },
  favoriteImage: {
    flex: 1,
    width: null,
    height: null,
  },
  shareButtonAndroid: {
    position: 'absolute',
    right: 15,
    bottom: 20,
    borderRadius: 50,
    width: 50,
    height: 50,
    backgroundColor: colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shareButtonIos: {
    marginRight: 15,
  },
});

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.favoritesFilm,
  };
};

export default connect(mapStateToProps)(MovieDetail);
