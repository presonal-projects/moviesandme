import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  ActivityIndicator,
  Text,
  Platform,
} from 'react-native';
import { Button } from 'react-native-elements';
import { AntDesign } from '@expo/vector-icons';
import { connect } from 'react-redux';

import MovieList from './MovieList';
import {
  getMoviesFromApiWithSearchedText,
  getPopularMoviesFromApi,
} from '../API/TMDB';
import colors from '../Constant/color';

class Search extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.totalPages = 0;
    this.searchFilms = '';
    this.state = {
      films: [],
      isReady: true,
      noResult: false,
    };
  }

  async componentDidMount() {
    this.setState({ isReady: false });
    await getPopularMoviesFromApi().then((data) => {
      this.setState({
        films: data.results,
        isReady: true,
      });
    });
  }

  _loadMovies = () => {
    if (this.searchFilms.length > 0) {
      this.setState({ isReady: false, noResult: false });
      getMoviesFromApiWithSearchedText(this.searchFilms, this.page + 1).then(
        (data) => {
          this.page = data.page;
          this.totalPages = data.total_pages;
          this.setState(
            {
              films: [...this.state.films, ...data.results],
              isReady: true,
            },
            () => {
              if (this.state.films.length === 0) {
                this.setState({ noResult: true });
              } else {
                this.setState({ noResult: false });
              }
            }
          );
        }
      );
    }
  };

  _updateSearch = (textInput) => {
    this.searchFilms = textInput;
  };

  _searchMovies = () => {
    this.page = 0;
    this.totalPages = 0;
    this.setState(
      {
        films: [],
      },
      () => this._loadMovies()
    );
  };

  _displayLoading = () => {
    if (!this.state.isReady) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={colors.blue} size='large' />
        </View>
      );
    }
  };

  _displayNoResult() {
    if (this.state.noResult) {
      return (
        <View style={styles.noResult}>
          <AntDesign name='exclamationcircleo' size={30} color={colors.blue} />
          <Text style={styles.noResultText}>No result for your search</Text>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <TextInput
            style={styles.textInput}
            placeholder='Titre du film'
            placeholderTextColor={colors.grey}
            selectionColor={colors.blue}
            onSubmitEditing={() => this._searchMovies()}
            onChangeText={(textInput) => this._updateSearch(textInput)}
          />
          <Button
            icon={
              <AntDesign
                name='search1'
                size={20}
                color='white'
                style={{ marginRight: 10 }}
              />
            }
            iconLeft
            title='Rechercher'
            onPress={() => this._searchMovies()}
          />
        </View>
        <View style={styles.resultContainer}>
          <MovieList
            films={this.state.films}
            navigation={this.props.navigation}
            loadFilms={this._loadMovies}
            page={this.page}
            totalPages={this.totalPages}
          />
        </View>
        {this._displayLoading()}
        {this._displayNoResult()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
  },
  searchContainer: {
    flex: 1,
    marginBottom: 10,
  },
  resultContainer: {
    ...Platform.select({
      ios: {
        flex: 5,
      },
      android: {
        flex: 4,
      },
    }),
  },
  textInput: {
    marginVertical: 20,
    paddingHorizontal: 10,
    fontSize: 19,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noResult: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noResultText: {
    fontFamily: 'PT',
    fontSize: 16,
    marginTop: 5,
  },
  loadingContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    favoritesFilm: state.favoritesFilm,
  };
};

export default connect(mapStateToProps)(Search);
