import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';

import colors from '../Constant/color';
import { getImageFromApi } from '../API/TMDB';
import FadeIn from '../Animation/FadeIn';

export default class MoviesItem extends Component {
  _displayFavoriteImage() {
    if (this.props.isFavorite) {
      return (
        <Image
          source={require('../Assets/Images/ic_favorite.png')}
          style={styles.favoriteImage}
        />
      );
    }
  }

  render() {
    const { movie, displayDetailForFilm } = this.props;

    return (
      <FadeIn>
        <TouchableOpacity
          style={styles.item}
          onPress={() => displayDetailForFilm(movie.id, movie.title)}
        >
          <Image
            source={{
              uri: getImageFromApi(movie.poster_path),
            }}
            style={styles.image}
          />
          <View style={styles.meta}>
            <View style={styles.title}>
              {this._displayFavoriteImage()}
              <Text style={styles.titleText}>{movie.original_title}</Text>
              <Text style={styles.rateText}>{movie.vote_average}</Text>
            </View>
            <View style={styles.desc}>
              <Text numberOfLines={5} style={styles.overviewText}>
                {movie.overview}
              </Text>
            </View>
            <View style={styles.release}>
              <Text style={styles.releaseText}>
                Release date {movie.release_date}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </FadeIn>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 10,
  },
  meta: {
    flex: 1,
  },
  image: {
    width: 120,
    height: 180,
    marginRight: 10,
  },
  title: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  titleText: {
    width: '75%',
    flexWrap: 'wrap',
    fontFamily: 'PT_Bold',
  },
  desc: {
    flex: 2,
  },
  release: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'flex-end',
  },
  rateText: {
    fontFamily: 'PT',
    fontSize: 20,
    color: colors.grey,
  },
  releaseText: {
    fontFamily: 'PT',
  },
  overviewText: {
    fontFamily: 'PT',
  },
  separator: {
    height: 1,
    backgroundColor: colors.grey,
  },
  favoriteImage: {
    width: 20,
    height: 20,
  },
});
