import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { AntDesign } from '@expo/vector-icons';

import colors from '../Constant/color';
import Search from '../Components/Search';
import MovieDetail from '../Components/MovieDetail';
import Favorites from '../Components/Favorites';
import Test from '../Components/Test';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const StackSearchRoot = () => (
  <Stack.Navigator
    initialRouteName='Search'
    screenOptions={{
      headerStyle: { backgroundColor: colors.blue },
      headerTitleStyle: { color: colors.white },
    }}
  >
    <Stack.Screen
      name='Search'
      component={Search}
      options={{ title: 'Rechercher' }}
    />
    <Stack.Screen
      name='Detail'
      component={MovieDetail}
      options={({ route }) => ({
        title: route.params.movieTitle,
        headerTintColor: colors.white,
      })}
    />
  </Stack.Navigator>
);

const StackFavoritesRoot = () => (
  <Stack.Navigator
    initialRouteName='Favorites'
    screenOptions={{
      headerStyle: { backgroundColor: colors.blue },
      headerTitleStyle: { color: colors.white },
    }}
  >
    <Stack.Screen
      name='Favorites'
      component={Favorites}
      options={{ title: 'Favorites list' }}
    />
    <Stack.Screen
      name='Detail'
      component={MovieDetail}
      options={({ route }) => ({
        title: route.params.movieTitle,
        headerTintColor: colors.white,
      })}
    />
  </Stack.Navigator>
);

const StackTestRoot = () => (
  <Stack.Navigator
    initialRouteName='Test'
    screenOptions={{
      headerStyle: { backgroundColor: colors.blue },
      headerTitleStyle: { color: colors.white },
    }}
  >
    <Stack.Screen
      name='Test'
      component={Test}
      options={{ title: 'Test Screen' }}
    />
  </Stack.Navigator>
);

const TabRoot = () => (
  <SafeAreaProvider>
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused }) => {
            let iconName;

            if (route.name === 'Search') {
              iconName = 'search1';
            } else if (route.name === 'Favorites') {
              iconName = focused ? 'heart' : 'hearto';
            } else if (route.name === 'Test') {
              iconName = focused ? 'questioncircle' : 'questioncircleo';
            }

            return <AntDesign name={iconName} size={20} color={colors.blue} />;
          },
        })}
        headerMode='none'
        tabBarOptions={{
          activeTintColor: colors.blue,
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name='Search' component={StackSearchRoot} />
        <Tab.Screen name='Favorites' component={StackFavoritesRoot} />
        <Tab.Screen name='Test' component={StackTestRoot} />
      </Tab.Navigator>
    </NavigationContainer>
  </SafeAreaProvider>
);

export default TabRoot;
