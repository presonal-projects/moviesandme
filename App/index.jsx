import React, { Component } from 'react';
import { View, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { ThemeProvider } from 'react-native-elements';
import * as Font from 'expo-font';
import { Provider } from 'react-redux';

import Store from './Store/configureStore';
import colors from './Constant/color';
import Navigation from './Navigation/Navigation';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      PT: require('./Assets/Fonts/PTSans-Regular.ttf'),
      PT_Bold: require('./Assets/Fonts/PTSans-Bold.ttf'),
    });
    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return (
        <ThemeProvider theme={theme}>
          <View style={styles.containerActivity}>
            <ActivityIndicator color={colors.blue} size={50} />
          </View>
        </ThemeProvider>
      );
    }

    return (
      <ThemeProvider theme={theme}>
        <Provider store={Store}>
          <StatusBar barStyle='light-content' translucent />
          <Navigation />
        </Provider>
      </ThemeProvider>
    );
  }
}

const theme = {
  colors: {
    primary: colors.blue,
  },
  Button: {
    titleStyle: {
      fontFamily: 'PT_Bold',
    },
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerActivity: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'PT',
  },
});
